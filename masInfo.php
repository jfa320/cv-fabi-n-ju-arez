<?php

include 'includes/funciones/funciones.php';
include 'includes/templates/header.php';


?>

<div class="fondoInfo parallax">
    <div class="container">
        <div class="estudios animacionFadeIn">
            <div class="descripcionEstudios ">

                <p>
                    Comencé mis estudios de la carrera <span class="bold">Tecnicatura en Informática</span> en el año 2015 en la Universidad Nacional de General Sarmiento. Me recibí en 2018 y desde entonces seguí estudiando la carrera <span class="bold">Licenciatura en Sistemas</span> en el mismo establecimiento.<br>
                    Además, por mi cuenta empecé a estudiar áreas específicas de software que me interesan, como lo es el <span class="bold">desarrollo web</span>.<br>
                </p>
            </div>
            <a href="https://www.ungs.edu.ar/" target="_blank"><img src="img/logosRedes/logoungs.png" alt="Logo Universidad"></a>
        </div>
        <div class="fotoConEpigrafe animacionFadeIn">
            <img src="img/fotos/fotograduacion.jpg" alt="Foto Graduacion">
            <div class="epigrafeFoto">
                <p>Graduación como Técnico Superior en Informática junto a uno de mis mejores profesores.
                </p>
            </div>
        </div>

    </div>

</div>
<h3 class="titulares centrarTexto">Lenguajes de Programación y Tecnologías Conocidos</h3>
<div class="fondo2 parallax">
    <div class="container contenedorCaja ">
        <div class="tecnologiasConocidas">
            <nav class="menuTecnologias ">
                <a href="#tecnologiasWeb" id='btnTecnoWeb'> <i class="fas fa-globe-americas"></i> <small> Web</small></a>
                <a href="#tecnologiasEscritorio" id='btnTecnoEscritorio'><i class="fas fa-desktop"></i><small> Escritorio</small> </a>
                <a href="#tecnologiasBD" id='btnTecnoBD'><i class="fas fa-database"></i><small> Bases de Datos</small> </a>
            </nav>
            <div id='tecnologiasWeb' class="ocultar contenedorSeccionesMenu">
                <section class="frontendLenguajes">
                    <h3 class="centrarTexto">Frontend</h3>
                    <div class="imgTecnologias">
                        <a href="#tecnologia1" class="tecnologiaLink "><img src="img/logosTecnologias/html5.png" alt="HTML5"></a>
                        <a href="#tecnologia2" class="tecnologiaLink "><img src="img/logosTecnologias/css3.png" alt="CSS3"></a>
                        <a href="#tecnologia3" class="tecnologiaLink "><img src="img/logosTecnologias/js.png" alt="JavaScript"></a>
                        <a href="#tecnologia13" class="tecnologiaLink "><img src="img/logosTecnologias/sass.png" alt="Sass"></a>

                    </div>

                </section>
                <section class=" backendLenguajes">
                    <h3 class="centrarTexto">Backend</h3>
                    <div class="imgTecnologias">
                        <a href="#tecnologia4" class="tecnologiaLink"><img src="img/logosTecnologias/php.png" alt="PHP"></a>
                        <a href="#tecnologia5" class="tecnologiaLink"><img src="img/logosTecnologias/nodeJs.png" alt="NodeJS"></a>
                    </div>
                </section>
                <section class=" backendLenguajes">
                    <h3 class="centrarTexto">Frameworks</h3>
                    <div class="imgTecnologias">
                        <a href="#tecnologia11" class="tecnologiaLink"><img src="img/logosTecnologias/react.png" alt="React.js"></a>
                    </div>
                </section>

            </div>

            <div id='tecnologiasEscritorio' class="ocultar contenedorSeccionesMenu">
                <div class=" imgTecnologias flexBlockStyle ">
                    <div class="contenedorLogo">
                        <a href="#tecnologia6" class="tecnologiaLink"><img src="img/logosTecnologias/java.png" id="javaLogo" class="imgTecnologiasEscritorio" alt="Java"></a>
                        <p class="logoTexto">Java</p>


                    </div>
                    <div class="contenedorLogo">
                        <a href="#tecnologia7" class="tecnologiaLink"><img src="img/logosTecnologias/python.png" class="imgTecnologiasEscritorio" id="pythonLogo" alt="Python"></a>
                        <p class="logoTexto">Python</p>
                    </div>

                    <div class="contenedorLogo">
                        <a href="#tecnologia8" class="tecnologiaLink"><img src="img/logosTecnologias/c.png" class="imgTecnologiasEscritorio" id="cLogo" alt="C"></a>
                        <p class="logoTexto">C</p>
                    </div>

                </div>
            </div>
            <div id='tecnologiasBD' class="ocultar contenedorSeccionesMenu">
                <a href="#tecnologia9" class="tecnologiaLink"> <img src="img/logosTecnologias/mysql.png" class="imgTecnologiasEscritorio" id="mysqlLogo" alt="Mysql Logo"></a>
                <a href="#tecnologia10" class="tecnologiaLink"> <img src="img/logosTecnologias/postgresql.png" class="imgTecnologiasEscritorio" id="postgresqlLogo" alt="Postgresql logo"></a>
                <a href="#tecnologia12" class="tecnologiaLink"> <img src="img/logosTecnologias/mongo.png" class="imgTecnologiasEscritorio" id="mongoDbLogo" alt="MongoDb logo"></a>
            </div>
            <div style="display:none;">
                <?php
                $i = 0;
                while ($i < 13) : //Esto está muy hardcodeado - cantidad de tecnologias que muestro
                    mostrarInfoTecnologiaNumero($i);
                    $i++;
                endwhile;
                ?>
            </div>
        </div>
    </div>
</div>
<h3 class="titulares centrarTexto">Cursos Realizados</h3>
<div class="fondo3 parallax">
    <div class="container contenedorCaja   ">
        <div class="cursos padding0">
            <ul class="cursosLista">
                <li class='curso'>
                    <p>
                        JavaScript Moderno - Agosto 2020 - Udemy
                    </p>

                    <div class="centradorFlex"><a href="https://www.udemy.com/certificate/UC-90677313-b55a-4793-8a7d-d0dc716e9250/" target="_blank"><img src="img/logosCursos/udemy.png" alt="" class="cursoImg"></a></div>

                </li>
                <li class='curso'>
                    <p>
                        Desarrollo Web con HTML5, CSS3, JS, AJAX, PHP y MySQL - Diciembre 2019 - Udemy
                    </p>

                    <div class="centradorFlex"><a href="https://www.udemy.com/certificate/UC-13e42951-4e76-410e-bf3f-a4e93c80ca8f/" target="_blank"><img src="img/logosCursos/udemy.png" alt="" class="cursoImg"></a></div>

                </li>

                <li class='curso'>
                    <p> Liderazgo y Oratoria - Diciembre 2019 -
                        Sede San Miguel - Cnel. Charlone 694,
                        San Miguel, Buenos Aires</p>
                    <div class="centradorFlex"><a href="http://liderazgoyoratoria.com.ar/" target="_blank"><img src="img/logosCursos/liderazgo.png" alt="" class="cursoImg"></a></div>
                </li>
                <li class='curso'>
                    <p> First Certificate in English -
                        Agosto 2016 - AACI , Suipacha 1333,CABA</p>
                    <div class="centradorFlex">
                        <a href="https://www.aaci.org.ar/" target="_blank"><img src="img/logosCursos/aaci.png" alt="" class="cursoImg"></a>
                    </div>
                </li>

            </ul>
        </div>
    </div>
</div>
<?php include 'includes/templates/footer.php' ?>