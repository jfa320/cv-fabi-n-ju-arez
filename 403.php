<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Acceso Prohibido</title>
    <style>
        html {
            display: table;
            font-family: sans-serif;
            height: 100%;
            text-align: center;
            width: 100%;
        }

        body {
            display: table-cell;
            vertical-align: middle;
            margin: 2em auto;
            background-color: #e5deea;
        }

        h1 {
            color: #000;
            font-size: 2em;
            font-weight: 400;
        }

        p {
            margin: 0 auto;
            color: #000;
            width: 280px;
        }

        a {
            color: #f08629;
            text-decoration: none;
        }

        img {
            width: 15%;
        }

        @media(max-width:600px) {
            img {
                width: 35%;
            }
        }
    </style>
</head>

<body>
    <img src="img/404/emoji.png" alt="Emoji Triste">
    <h1>Acceso Prohibido</h1>
    <p>Puedes volver al sitio presionando <a href="index">aquí</a>.</p>

</body>

</html>