<?php include 'includes/templates/header.php' ?>

<div class="fondoContacto parallax">
    <div class="container contenedorCaja ">
        <form id="contactoMail" action="mailContacto.php" method="POST" class="formularioContacto animacionFadeIn" novalidate>
            <legend>Me contactaré a la brevedad</legend>

            <div class="labelInput">
                <label for="nombre">Nombre: </label>
                <input type="text" name="nombre" id="nombre" placeholder="Nombre">
            </div>
            <div class="labelInput">
                <label for="apellido">Apellido: </label>
                <input type="text" name="apellido" id="apellido" placeholder="Apellido">
            </div>
            <div class="labelInput">
                <label for="email">Email: </label>
                <input type="email" name="email" id="email" placeholder="Email">
            </div>
            <div class="labelInput">
                <label for="mensaje">Mensaje: </label>
                <textarea class="mensaje" name="mensaje" id="mensaje" placeholder="Su mensaje, por favor..." col='40' row='10'></textarea>
            </div>

            <input type="submit" value="ENVIAR" id="btnEnviarContacto" class="btnEnviarContacto ">

        </form>
    </div>
    <div class="contenedorPadding">
        <section class=" containerContactoDirecto animacionFadeIn">
            <h2>Contacto Directo</h3>
                <div class="datosContacto">
                    <p><i class="fas fa-mobile-alt"></i> 1166825806</p>
                    <p><i class="fas fa-envelope"></i> juarefa320@gmail.com</p>
                </div>
        </section>
    </div>


</div>
<?php include 'includes/templates/footer.php' ?>