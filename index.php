<?php include 'includes/templates/header.php' ?>


<div class="fondo parallax">
    <div class="informacionPrincipal animacionFadeIn container">
        <img src="img/fotos/perfil.png" alt="Imagen CV Fabián Juárez">

        <div class="descripcionPerfil">
            <p class="textoPerfil">T&eacute;cnico Superior en Inform&aacute;tica recibido en la <a class="universidadEnlace" target="_blank" href="https://www.ungs.edu.ar/">Universidad Nacional
                    de
                    General Sarmiento</a>.
            </p>
            <p class="textoPerfil">Estudiante de la carrera Licenciatura en Sistemas en el mismo
                establecimiento.
            </p>
            <p class="textoPerfil">Desarrollador Java - Diseñador Web
            </p>
        </div>
    </div>
</div>
<?php include 'includes/templates/footer.php' ?>