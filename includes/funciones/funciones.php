<?php

function mostrarInfoTecnologiaNumero($i)
{
    echo ('tecnologia' . $i);
    echo ("<div class='tecnologiaLink tecnologiaLinkCaja centrarTexto' id=tecnologia" . ($i + 1) . '>');

    switch ($i) {
        case 0:
            armarCajaInfoTecnologia('HTML5', 'html5', "Comencé a aprender HTML en la universidad aunque me especializé por mi cuenta. Conozco bastante del lenguaje y podria considerarse que tengo un <span class='bold'>nivel intermedio </span> en él. ", 'En esta página lo use para maquetar basicamente y luego refactoricé todo con PHP para evitar repetición de código.');
            break;
        case 1:
            armarCajaInfoTecnologia('CSS3', 'css3', "Conozcó lo fundamental (incluyendo media queries para responsive design) más algunos añadidos nuevos de CSS3 como el uso de variables o animaciones. Consideró que tengo un <span class='bold'>nivel intermedio </span> en el.", 'En esta página lo usé para dar los estilos básicos,además que agregue transiciones y animaciones (como parallax a los fondos). Además, aproveche las media queries para hacer el sitio responsivo para una buena visualización en celulares,tablets o computadoras. ');
            break;
        case 2:
            armarCajaInfoTecnologia('JavaScript', 'js', "Este lenguaje es fundamental para el frontend. Uso AJAX para hacer peticiones asíncronas para no tener la necesidad de refrescar la pantalla al hacer alguna. Me manejo bastante bien en él y creo que tengo un <span class='bold'>nivel intermedio </span>.", 'En este sitio usé jQuery aunque tambien manejo los métodos y funciones nativos de Javascript.');
            break;
        case 3:
            armarCajaInfoTecnologia('PHP', 'php', 'En lo que respecta a desarrollo web, es el lenguaje de backend que más domino. Sin embargo debo aprender un poco más del paradigma orientado a objetos en él y de algún framework como Laravel o Symfony.', 'En este sitio las páginas estan realizadas con él para no repetir la cabecera y el pie de página. Además se encarga de hacer que el formulario de contacto me envíe un mail con el mensaje que me hayan dejado, entre otras funciones.');
            break;
        case 4:
            armarCajaInfoTecnologia("Node.js", 'nodeJs', "Mi nivel en él es <span class='bold'> bajo </span> aunque no soy principiante. Me manejo mejor al usarlo con Express. ", '');
            break;
        case 5:
            armarCajaInfoTecnologia("Java", 'java', "Es el lenguaje que aprendí en la Universidad y el que más conozco. He desarrollado principalmente aplicaciones de escritorio usando Swing como frontend. En él me muevo bien al usar el paradigma orientado a objetos. ", "He desarrollado aplicaciones como agendas, juegos de tipo rompecabezas, organizadores de cursos para una institucion educativa, entre otros. Mi nivel en él es <span class='bold'> intermedio </span>. ");
            break;
        case 6:
            armarCajaInfoTecnologia("Python", 'python', "Fue el primer lenguaje que aprendí, sin embargo no continúe en él. Tengo un nivel <span class='bold'> bajo </span>.", '');
            break;
        case 7:
            armarCajaInfoTecnologia("C", 'c', 'Conozcó aspectos un poco más allá de lo básico pero tampoco me especialicé tanto.', '');
            break;
        case 8:
            armarCajaInfoTecnologia("MySQL", 'mysql', 'En lo que respecta a Bases de Datos, es el lenguaje en el que tengo más conocimientos.', 'Sé como usar comandos SQL, normalización y el armado de Diagramas Entidad-Relacion (DER).');
            break;
        case 9:
            armarCajaInfoTecnologia("PostgreSQL", 'postgresql', 'Este lenguaje lo aprendí en la universidad aunque no lo seguí usando en mis proyectos.', 'Sé como usar comandos SQL, normalización y el armado de Diagramas Entidad-Relacion (DER).');
            break;
        case 10:
            armarCajaInfoTecnologia("React", 'react', 'Lo aprendí recientemente y aún me falta algo de práctica. Sin embargo soy capaz de usarlo.', 'Esta tecnología me interesa mucho y espero capacitarme más en ella.');
            break;
        case 11:
                armarCajaInfoTecnologia("MongoDB", 'mongo', 'Aprendí este tipo de base de datos (no relacional) en la universidad.', 'Me pareció excelente pero debo aplicarla en un proyecto más grande para ver su potencial.');
                break;
        case 12:
                armarCajaInfoTecnologia("Sass", 'sass', 'Me encanta usar este preprocesador CSS. Desde que lo aprendí no deje de usarlo.', 'No me considero un experto pero creo usarlo bastante bien.');
                break;
        default:
            echo 'Error';
    }
};

function armarCajaInfoTecnologia($titulo, $imgNombre, $parrafo1, $parrafo2)
{
    echo "<h2 class=' tituloCajaTecnologia'>$titulo</h2>";
    echo "<img src='img/logosTecnologias/$imgNombre.png' alt='Imagen $titulo'>";
    echo "<p class='infoCajaTecnologia'>$parrafo1</p> <br> ";
    echo "<p class='infoCajaTecnologia'>$parrafo2</p></div>";
}
