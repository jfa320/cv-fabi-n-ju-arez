<section class="redesSociales">
    <h2 class="centrarTexto">Redes Sociales</h2>
    <div class="logosRedes">
        <!--enlazar a cuentas-->
        <a href="https://www.linkedin.com/in/fabi%C3%A1n-ju%C3%A1rez-a320/" target="_blank"><img src="img/logosRedes/linkedin.png" alt="linkedin logo"></a>
        <a href="https://gitlab.com/jfa320" target="_blank"><img src="img/logosRedes/gitlab.png" alt="Gitlab logo"></a>
        <a href="https://github.com/jfa320" target="_blank"><img src="img/logosRedes/github.png" alt="Github logo"></a>

    </div>

</section>

<footer class="copyright">

    <p class="centrarTexto"> <a href="cc" style="color:white;">Todos los derechos reservados <?php echo date("Y"); ?> <img style="width: 1%;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg" alt="CC Logo"></a> </p>
</footer>


<script src="https://kit.fontawesome.com/f63130807c.js" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.6.4/jquery.colorbox-min.js" integrity="sha256-QbxNT+iBOdbuiav8squsceFDDYXb/8C+fI9r029M7X4=" crossorigin="anonymous"></script>

<script src="js/main.js"></script>

</body>

</html>