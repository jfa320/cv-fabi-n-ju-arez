<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="icon" href="img/favicon.png" type="image/png" />
    <link rel="stylesheet" href="css/normalize.css">
    <link href="https://fonts.googleapis.com/css?family=Crimson+Text:400,700|Noto+Sans&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,300;0,400;1,300;1,400&display=swap" rel="stylesheet"> 
    <script src="https://kit.fontawesome.com/f63130807c.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/colorbox.css">

    <link rel="stylesheet" href="css/style.css">
    <title><?php
            $archivo = basename($_SERVER['PHP_SELF']);
            $pagina = str_replace(".php", "", $archivo);
            if ($pagina == 'masInfo') {
                echo 'Más Información';
            } else if ($pagina == 'contacto') {
                echo 'Contacto';
            } else {
                echo 'Fabián Juárez';
            }
            ?>
    </title>
</head>

<body>
    <header class="menu cabecera">

        <div class=" contenedorBarra container">
            <a href="/" id='logoHome' class="logoHome">Fabi&aacute;n Ju&aacute;rez</a>

            <div class="menuMovil centrarTexto"><i class="fas fa-bars "></i></div>

            <nav class="navegacionPrincipal">
                <a href="masInfo">M&aacute;s info</a>
                <!--<a href="#">Proyectos</a> -->
                <a href="contacto">Contacto</a>
            </nav>

        </div>


    </header>