$(document).ready(function () {


    //Menu Movil
    $('.menuMovil').on('click', toggleMenu);

    function toggleMenu() {
        $('.navegacionPrincipal').slideToggle();
    }

    eventosInputsForm();

    //Menu Tecnologia
    //Pongo valor por default al menu de tecnologias
    if (window.screen.width >= 768) {

        $('.tecnologiasConocidas .contenedorSeccionesMenu:first').show().css("display", "flex");
    } else {

        $('.tecnologiasConocidas .contenedorSeccionesMenu:first').show();
    };

    $('.menuTecnologias a:first').addClass('seleccionMenu');

    $('.menuTecnologias a').on('click', cambiarMenuTecnologias);

    function cambiarMenuTecnologias() {
        $('.menuTecnologias a').removeClass('seleccionMenu');
        $(this).addClass('seleccionMenu');

        $('.ocultar').hide();
        var enlace = $(this).attr('href');

        if (window.screen.width >= 768) {
            $(enlace).fadeIn(1000).css("display", "flex");
        } else {

            $(enlace).fadeIn(1000);
        };
        return false;

    }

    //menu fijo

    var barraAltura = $('.menu').innerHeight();
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > 0) {
            $('.menu').addClass('fixed');
            $('body').css({
                'margin-top': barraAltura + 'px'
            });

        } else {
            $('.menu').removeClass('fixed');
            $('body').css({
                'margin-top': '0px'
            })
        }
    });

    //AJAX contacto
    $('#contactoMail').on('submit', function (e) {
        e.preventDefault();
        limpiarInputs();
        var datos = $(this).serializeArray();

        if (!faltanDatosPorCompletar(datos)) {
            if (mailValido(datos[2].value)) {
                $("#btnEnviarContacto").prop('disabled', 'true');
                $("#btnEnviarContacto:hover").css('cursor', 'not-allowed');
                $("#btnEnviarContacto").addClass('cargando');
                $("#btnEnviarContacto").attr('value', '');
                $.ajax({
                    type: $(this).attr('method'),
                    data: datos,
                    url: $(this).attr('action'),
                    dataType: 'json',
                    success: function (data) {
                        var resultado = data;

                        if (resultado.respuesta === 'exito') {
                            Swal.fire(
                                'Correcto',
                                'Envio correcto, pronto recibirá su respuesta',
                                'success'
                            );
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: 'Hubo un error,vuelva a intentarlo'
                            });
                        }
                        $("#btnEnviarContacto").prop('disabled', 'false');
                        $('#btnEnviarContacto').removeAttr('disabled');
                        $("#btnEnviarContacto:hover").css('cursor', 'pointer');
                        $("#btnEnviarContacto").removeClass('cargando');
                        $("#btnEnviarContacto").attr('value', 'ENVIAR');
                    }
                });
            } else {
                marcarComoInvalido(datos[2]);
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'El email ingresado no es válido'
                });

            }

        } else {
            Swal.fire({
                icon: 'error',
                title: 'Error',
                html: 'Hay campos incompletos! <br> Todos los campos son obligatorios.'
            });
        }

    });

    //colorbox

    $(".tecnologiaLink").colorbox({

        inline: true,
        width: "80%"
    });
    $('#cboxWrapper').on('click', function () {
        $.colorbox.close();
    });

    function faltanDatosPorCompletar(datos) {
        var resultado = false;
        for (indice in datos) {
            if (datos[indice].value == '') {
                marcarComoInvalido(datos[indice]);
                resultado = true;
            };
        }
        return resultado;
    }

    function marcarComoInvalido(dato) {
        if (dato.name == 'mensaje') {
            $('#contactoMail textarea').css('border', '1px solid #fc6565');
        } else {
            $('#contactoMail input[id=' + dato.name + ']').css('border', '1px solid #fc6565');
        }
    }

    function limpiarInputs() {
        $('#contactoMail textarea').css('border', 'none').fadeIn();
        $('#contactoMail input').css('border', 'none').fadeIn();
    }

    function mailValido(mail) {
        var regexMail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        var esEmail = regexMail.test(mail);
        return esEmail;
    }

    function eventosInputsForm() {
        $('form#contactoMail input').on('keyup', (event) => {
            if (event.target.value != '') {
                var input = event.target;
                $(input).css('border', 'none');
            };
        })
        $('form#contactoMail textarea').on('keyup', () => {
            var textArea = event.target;
            $(textArea).css('border', 'none');
        })
    }

});