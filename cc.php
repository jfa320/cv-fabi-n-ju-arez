<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Licencia Creative Commons</title>
    <style>
        html {
            display: table;
            font-family: sans-serif;
            height: 100%;
            text-align: center;
            width: 100%;
        }

        body {
            display: table-cell;
            vertical-align: middle;
            margin: 2em auto;
            background-color: #e5deea;
        }

        h1 {
            color: #000;
            font-size: 2em;
            font-weight: 400;
        }

        p {
            margin: 0 auto;
            color: #000;
            width: 300px;
        }

        a {
            color: #f08629;
            text-decoration: none;
        }


        @media(max-width:600px) {
            img {
                width: 35%;
            }
        }
    </style>
</head>

<body>
    <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br /></br>Esta obra
    está
    bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Licencia Creative Commons
        Atribución-NoComercial 4.0 Internacional. </a>

    <p></br>Puedes volver al sitio presionando <a href='/'>aquí</a>.</p>

</body>

</html>