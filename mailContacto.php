<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';
$config = parse_ini_file('config/config.ini', true);

$nombre = filter_var($_POST['nombre'], FILTER_SANITIZE_STRING);
$apellido = filter_var($_POST['apellido'], FILTER_SANITIZE_STRING);
$mailEnviador = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
$mailReceptor = 'juarefa320@gmail.com';
$asunto = utf8_decode("Contacto - Página Web Fabián Juárez - " . $nombre . " " . $apellido);

$mensaje = filter_var($_POST['mensaje'], FILTER_SANITIZE_STRING);
$mensaje .= "<br> ";
$mensaje .= "<br> <a href='mailto:$mailEnviador?subject=Re: $asunto' style='text-decoration:underline;'>$mailEnviador</a>";
$mensaje .= "<br> $nombre $apellido";
$mensaje .= "<br><br> <strong>No responder este mail</strong>";



$mail = new PHPMailer();

//Luego tenemos que iniciar la validación por SMTP:
$mail->IsSMTP();
$mail->SMTPAuth = true;
$mail->Host = "smtp.gmail.com"; // SMTP a utilizar. Por ej. smtp.elserver.com
$mail->Username = $config['email']['username']; // Correo completo a utilizar
$mail->Password =  $config['email']['password'];  // Contraseña
$mail->Port = 587; // Puerto a utilizar
$mail->SMTPSecure = 'tls';
//Con estas pocas líneas iniciamos una conexión con el SMTP. Lo que ahora deberíamos hacer, es configurar el mensaje a enviar, el //From, etc.

$mail->setFrom($mailEnviador, $nombre . " " . $apellido);

$mail->AddAddress($mailReceptor); // Esta es la dirección a donde enviamos
$mail->IsHTML(true); // El correo se envía como HTML
$mail->Subject = $asunto; // Este es el titulo del email.
$mail->Body = $mensaje; // Mensaje a enviar


//También podríamos agregar simples verificaciones para saber si se envió:
if ($mail->Send()) {
    $respuesta = array(
        'respuesta' => 'exito'
    );
} else {
    $respuesta = array(
        'respuesta' => 'error'
    );
}
die(json_encode($respuesta));
